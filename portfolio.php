<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  $title = 'PORTFOLIO';  ?>
	<?php  require('portfolio-carousal.php');  ?>
<style type="text/css">
	.carousel-caption {
		  bottom: 35% !important;
		  right: unset;
		  left: unset;
		  width: 100%;
		  background-color: #00000088;
		  padding-top: 2.25rem;
		  padding-bottom: unset;
		}
</style>
	<div class="section">
		<div class="container">
			<div class="col-md-12 col-sm-12 row mt-5 mb-5">
    			<?php
        			$dirname = "images/new/portfolio/";
					$images = glob($dirname."*.jpg");
					foreach($images as $image) {
					    echo '<img data-aos="zoom-in" class="title col-md-4 mb-4" src="'.$image.'">';
					}
				?>
    		</div>

		</div>
	</div>
<?php  require('footer.php');  ?>
</body>
</html>
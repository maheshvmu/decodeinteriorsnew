<footer class="open-sans pb-2">
	<div class="row" style="--bs-gutter-x:0">
		<div class="p-5 row color-grey col-md-5 justify-content-between">
			<div class="col-md-12 col-sm-12 text-white">
				<img src="images/new/decode_white.png" class="p-5 pb-3" style="width: 300px; padding-left: 0 !important;">
				<h5 class="fw-bold">GET IN TOUCH</h5>
				<p class=" mt-1"><span class="pb-1">Our vision is to provide high quality, personalised, artisanal furniture and decor that is made in India with the most rigorous specifications and attention to detail to our exacting and refined clientele.</p>
			</div>

			<div class="col-md-12 col-sm-12 text-white" id="contact-us">
				<ul class="footer-ul p-5 pt-3">
					<h4 class="pb-4">CONTACT US</h4>
					<h6 class="pb-3">
						<div class="row">
			                <i class="fa fa-phone-square col-md-1"></i>
			                <a class="pl-2 col-md-11 text-white text-decoration-none">7548815999 | 7548814999</a>
			            </div>
		            </h6>
					<h6 class="pb-3">
						<div class="row">
							<i class="fa fa-envelope col-md-1"></i>
		                    <a class="pl-2 col-md-11 text-white text-decoration-none" href="mailto:info@decode.com">info@decode.com</a>
		                </div>
	                </h6>
					<h6 class="pb-3">
						<div class="row">
							<i class="fa fa-map-marker col-md-1"></i>
							<a class="pl-2 col-md-11 text-white text-decoration-none" href="#">Eldems Road, Vannia Teynampet, Lubdhi Colony, Teynampet, Chennai, Tamil Nadu 600018.
	                        New York</a>
	                    </div>
	                    
					</h6>
				</ul>
			</div>

			
			<hr class="color-white"> 
			<div class="row"> 
				<div class="col-md-12 text-white">&copy; 1996-2021 Transform Design. All Rights Reserved</div> 
				<!-- <div class="col-md-6 text-end">
					<a href="#"><i class="fa fa-facebook-square green-icon"></i></a> 
					<a href="#"><i class="fa fa-instagram green-icon"></i></a> 
					<a href="#"><i class="fa fa-envelope green-icon"></i></a>
				</div>  -->
			</div> 
		</div>
		<div class="py-0 row col-md-7 justify-content-between">
			 	<div class="col-md-12 card shadow open-sans mx-4" style="padding: 5% 15%;"> 
			 		<h5 class="green-text fw-bold">ENQUIRY</h5>
			 		<form>
			 			<div class="form-group col-md-6">
							<label class="pb-2" for="exampleInputPassword1">Name*</label>
							<input type="text" class="form-control" id="name">
						</div>
						<div class="form-group col-md-6">
							<label class="pb-2" for="exampleInputEmail1">Email*</label>
							<input type="email" class="form-control" id="email">
						</div>
						<div class="form-group col-md-6">
							<label class="pb-2" for="exampleInputPassword1">Mobile No*</label>
							<input type="text" class="form-control" id="mobile">
						</div>
						<div class="form-group ">
							<label class="pb-2" for="exampleInputPassword1">Message*</label>
							<textarea class="form-control" name="message" rows="6"></textarea>
						</div>
						
						<center><button type="submit" class="btn green-btn fw-bold">Submit Now</button></center>
					</form> 
			 	</div> </div>
	</div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>

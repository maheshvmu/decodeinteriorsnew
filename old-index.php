<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>

	<div class="section">
        <div class="container">

        	<div class="col-md-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
						  <div class="carousel-inner">
						    <div class="carousel-item active">
							    <div class="col-md-12 mt-5" data-aos="fade-up">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/1.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/2.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/3.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/4.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/5.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/6.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/7.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/8.jpg">
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						    </div>

						    <div class="carousel-item">
						    	<div class="col-md-12 mt-5" data-aos="fade-up">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/9.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/10.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/11.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/12.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/13.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/14.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/15.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100" src="images/new/gallery/16.jpg">
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						      <!-- <img src="decode-img3.png" class="d-block w-100" alt="..."> -->
						    </div>
						  </div>
						  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Previous</span>
						  </button>
						  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Next</span>
						  </button>
						</div>



						<center class="mb-4"><a class="text-center fs-5 green-btn">SEE MORE</a></center>
                    </div>
                    
                </div>
        	
            <div class="row justify-content-center">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">Welcome to DECODE</h2>
                        <p class="m-5 text-center">An extraordinary convergence of design and industry, the expression of contemporary approach. People, ideas, places, projects and products are what we make DECODE unique, now as ever.<br> An adventure that validates courageous vision. <br> A story and an identity that unwaveringly focuses on the future.</p>
                    </div>
                </div>
                <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">DECODE</h2>
                    	<p class="title text-center p-1">Contemporary international design</p>
                    	<center><img src="images/new/decode_division/Decode.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Contemporary design and timeless elegance. DECODE interprets contemporary culture and the evolution of living trends with premium quality furnishings designed to enhance indoor and outdoor areas. Timeless objects, international icons for design enthusiasts.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">HOSPITALITY</h2>
                    	<center><img src="images/new/decode_division/Hospitality.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">For the past fourty years Decodes Contract Division has designed and proposed turnkey projects and interior fit outs for the hospitality, retail</p>
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">CORPORATE</h2>
                    	<center><img src="images/new/decode_division/Corporate.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">For the past fourty years Decodes Contract Division has designed and proposed turnkey projects and interior fit outs for the hospitality, retail</p>
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">RESIDENCE</h2>
                    	<p class="title text-center p-1">Contemporary classic</p>
                    	<center><img src="images/new/decode_division/residential.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Tradition and authenticity enliven a collection of classical furnishings with powerful contemporary traits. Handcrafting expertise, exclusive details and precious materials characterise DECODE's pieces for sophisticated interior décor projects.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">Classique</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">RETAIL</h2>
                    	<p class="title text-center p-1">Contemporary outdoor design</p>
                    	<center><img src="images/new/decode_division/retail.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">DECODE takes the same values of quality, know how and design that make up the DNA of the brand outdoors. Design, innovation and creativity. Each DECODE Outdoor product has a hi-tech soul and a design identity. The drive to create new things is constant. Mechanisms, structures, materials all technically exceptional.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">Decode Outdoor</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">INDUSTRIAL</h2>
                    	<p class="title text-center p-1">Contemporary classic</p>
                    	<center><img src="images/new/decode_division/industrial.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Tradition and authenticity enliven a collection of classical furnishings with powerful contemporary traits. Handcrafting expertise, exclusive details and precious materials characterise DECODE's pieces for sophisticated interior décor projects.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">Classique</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">INSTITUTIONAL</h2>
                    	
                    	<center><img src="images/new/decode_division/institutional.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Tradition and authenticity enliven a collection of classical furnishings with powerful contemporary traits. Handcrafting expertise, exclusive details and precious materials characterise DECODE's pieces for sophisticated interior décor projects.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">Classique</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">The Historical Indian Brand</h2>
                    	<p class="title text-center p-1"></p>
                    	<center><img src="images/decode-img4.png" width="100%"></center>
                        <p class="m-5 mb-3 text-center">For the past forty years DECODE's Contract Division has designed and proposed turnkey projects and interior fit-outs for the hospitality, retail, office and nautical sectors. The company is acknowledged as an exceptional partner for décor fittings by designers and investors throughout the world for its know-how in terms of technical and executive development of hotels, residences, offices, theatres, and shops.</p>
						<center class="mb-4"><a class="text-center fs-5" href="#">DECODE Project Division</a></center>
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">News</h2>
                    	<p class="title text-center p-1"></p>
                    	<div class="row">
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Events & Fairs 2021</p>
                    			<img class="w-100" src="images/decode-news1.png">
                    			<p class="mt-2">2021 Indoor Collection</p>
                    		</div>
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Products News - 2021</p>
                    			<img class="w-100" src="images/decode-news2.png">
                    			<p class="mt-2">Heritage Perspectives 2021</p>
                    		</div>
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Feb 2021</p>
                    			<img class="w-100" src="images/decode-news3.png">
                    			<p class="mt-2">2021 Outdoor Collection</p>
                    		</div>
                    	</div>
						<center class="mb-4"><a class="text-center fs-5" href="#">All News</a></center>
                    </div>
                    <hr class="color-black">
                </div>
                <div class="col-md-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<img class="w-100" src="images/decode-img1.png">
                    	<div class="row">
                    		<div class="col-md-3 col-sm-12 p-3">
                    			<h2 class="title text-center p-2 mt-5">The culture of design, in your kitchen.</h2>
                    		</div>
                    		<div class="col-md-9 col-sm-12 p-3">
                    			<p class="mb-2 mt-5 text-center p-2">The luxury all-Indian brand, combines tradition with modernity, and culture with design. Founded in 19__ _, the company has always been devoted to the creation and production of innovative kitchens, intended as the centre of the home; a place to be lived in, used and shared. In 2019, DECODE established a strategy based on the common values of design, innovation and quality.</p>
                    			<p class="text-end mb-2"><a href="#">Read More</a></p>
                    		</div>
                    	</div>
                    </div>
                    <hr class="color-black">
                </div>
                <div class="col-md-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">Our Products</h2>
                    	
                        <p class="m-5 mb-3 fs-4 text-center product-sub">Explore the extensive range of DECODE furnishings. Solutions designed to meet the most diverse requirements of contemporary living.The perfect blend of innovation and design. An unmistakable style for timeless elegance.</p>


                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <!-- <img src="decode-img1.png" class="d-block w-100" alt="..."> -->
							     <div class="col-md-12 mt-5" data-aos="fade-up">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Events & Fairs 2021</p>
				                    			<img class="w-50" src="images/decode-news1.png">
				                    			<p class="mt-2">2021 Indoor Collection</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Products News - 2021</p>
				                    			<img class="w-50" src="images/decode-news2.png">
				                    			<p class="mt-2">Heritage Perspectives 2021</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Feb 2021</p>
				                    			<img class="w-50" src="images/decode-news3.png">
				                    			<p class="mt-2">2021 Outdoor Collection</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Feb 2021</p>
				                    			<img class="w-50" src="images/decode-news3.png">
				                    			<p class="mt-2">2021 Outdoor Collection</p></center>
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						    </div>
						    
						    <div class="carousel-item">
						    	<div class="col-md-12 mt-5" data-aos="fade-up">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Events & Fairs 2021</p>
				                    			<img class="w-50" src="images/decode-news1.png">
				                    			<p class="mt-2">2021 Indoor Collection</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Products News - 2021</p>
				                    			<img class="w-50" src="images/decode-news2.png">
				                    			<p class="mt-2">Heritage Perspectives 2021</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Feb 2021</p>
				                    			<img class="w-50" src="images/decode-news3.png">
				                    			<p class="mt-2">2021 Outdoor Collection</p></center>
				                    		</div>
				                    		<div class="col-md-6 col-sm-6 p-3">
				                    			<center><p class="mb-2">Feb 2021</p>
				                    			<img class="w-50" src="images/decode-news3.png">
				                    			<p class="mt-2">2021 Outdoor Collection</p></center>
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						      <!-- <img src="decode-img3.png" class="d-block w-100" alt="..."> -->
						    </div>
						  </div>
						  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Previous</span>
						  </button>
						  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Next</span>
						  </button>
						</div>



						<center class="mb-4"><a class="text-center fs-5" href="#">Product Finder</a></center>
                    </div>
                    <hr class="color-black">
                </div>
            </div>
        </div>
    </div>

    <?php  require('footer.php');  ?>
</body>
</html>
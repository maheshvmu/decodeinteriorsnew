<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>

	<div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">Browse through the collections</h2>
                        <p class="m-5 text-center">Creativity and innovation are the unmistakable features of Decode's style.<br> Refined contemporary design distinguishes domestic, office and outdoor settings.<br> Explore the collections, products and solutions designed to provide you with unique spaces.</p>
                        
                    </div>
                </div>
                <div class="col-md-12 mt-0" data-aos="fade-up">
	                <div class="row">
	            		<div class="col-md-4 col-sm-12 p-3">
	            			<p class="mb-2">Decode@Residence</p>
	            			<img class="w-100" src="images/decode-news1.png">
	            		</div>
	            		<div class="col-md-4 col-sm-12 p-3">
	            			<p class="mb-2">Decode@Retail</p>
	            			<img class="w-100" src="images/decode-news2.png">
	            		</div>
	            		<div class="col-md-4 col-sm-12 p-3">
	            			<p class="mb-2">Decode@Corporate</p>
	            			<img class="w-100" src="images/decode-news3.png">
	            		</div>
	            	</div>
	            </div>

                <div class="col-md-9 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">News</h2>
                    	<p class="title text-center p-1"></p>
                    	<div class="row">
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Events & Fairs 2021</p>
                    			<img class="w-100" src="images/decode-news1.png">
                    			<p class="mt-2">2021 Indoor Collection</p>
                    		</div>
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Products News - 2021</p>
                    			<img class="w-100" src="images/decode-news2.png">
                    			<p class="mt-2">Heritage Perspectives 2021</p>
                    		</div>
                    		<div class="col-md-4 col-sm-12 p-3">
                    			<p class="mb-2">Feb 2021</p>
                    			<img class="w-100" src="images/decode-news3.png">
                    			<p class="mt-2">2021 Outdoor Collection</p>
                    		</div>
                    	</div>
						<center class="mb-4"><a class="text-center fs-5" href="#">All News</a></center>
                    </div>
                </div>

                <hr class="color-black">

                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">Our Products</h2>
                    	<p class="title text-center fs-3 p-1">Innovation, design and quality. Explore the unmistakeable style of our furniture.</p>
                    	<div class="row">
                    		<div class="col-md-12 col-sm-12 row">
                    			<h2 class="title col-md-12 mb-4">Sofas</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png"> -->

                    			<?php
	                    			$dirname = "images/sofas/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>

                    			<h2 class="title col-md-12 mt-5 mb-4">Chairs</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png"> -->
                    			<?php
	                    			$dirname = "images/chairs/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>
                    		</div>
                    		<!-- <div class="col-md-6 col-sm-12 row">
                    			
                    		</div> -->
                    		<div class="col-md-12 col-sm-12 row">
                    			<h2 class="title col-md-12 mt-5 mb-4">Tables</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png"> -->
                    			<?php
	                    			$dirname = "images/tables/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>
                    		</div>
                    		<div class="col-md-12 col-sm-12 row">
                    			<h2 class="title col-md-12 mt-5 mb-4">Wardrobes</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png"> -->
                    			<?php
	                    			$dirname = "images/wardrobes/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>
                    		</div>
                    		<div class="col-md-12 col-sm-12 row">
                    			<h2 class="title col-md-12 mt-5 mb-4">Hospitality</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png"> -->
                    			<?php
	                    			$dirname = "images/hospitality/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>
                    		</div>
                    		<div class="col-md-12 col-sm-12 row">
                    			<h2 class="title col-md-12 mt-5 mb-4">Furniture</h2>
                    			<!-- <img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news1.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news3.png">
                    			<img class="title col-md-4 mb-2" src="images/decode-news2.png"> -->
                    			<?php
	                    			$dirname = "images/furniture/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<img class="title col-md-4 mb-2" src="'.$image.'">';
									}

								?>
                    		</div>
                    	</div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 mt-5 mb-5" data-aos="fade-up">
                	<div class="row">

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Projects</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 style="margin-top: auto; margin-bottom: auto;" class="card-title align-middle">Design Culture</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Product Libraby</h3>
							    </div>
							  </div>
							</div>
                		</div>

                	</div>
                </div>
            </div>
        </div>
    </div>

    <?php  require('footer.php');  ?>

</body>
</html>
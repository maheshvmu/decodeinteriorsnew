<div style="position: absolute;width: 100%; z-index: 2; margin-top: 50px;">
<!--  -->

	<nav class="navbar navbar-expand-lg navbar-light text-white" style="background: #fff;">
	  <div class="container-fluid">
	    <a class="navbar-brand" href="#"></a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse row justify-content-end" id="navbarNavDropdown">
	    	<div class="logo-div col-md-6" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400">
				<a class="navbar-brand" href="#"><img data-aos="zoom-in" src="images/new/decode_black.png" height="50px" > </a>
			</div>
	      <ul class="navbar-nav col-md-6" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="400">
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="index.php">HOME</a>
	        </li>
	        <li class="nav-item">
	         	<a class="nav-link" href="all-products.php">ALL PRODUCTS</a>
	        </li>
	        <li class="nav-item">
	         	<a class="nav-link" href="brand-vision.php">BRAND VISION</a>
	        </li>
	        <li class="nav-item">
	         	<a class="nav-link" href="portfolio.php">PORTFOLIO</a>
	        </li>
	        <li class="nav-item">
	         	<a class="nav-link" href="#clients">OUR CLIENTS</a>
	        </li>
	        <li class="nav-item">
	         	<a class="nav-link" href="#contact-us">CONTACT US</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<!-- <div>
		<h1 class="image-h1">Design is a journey of discovery</h1>
		<a class="badge badge-info">PORTFOLIO</a>
	</div> -->
</div>

	<!-- <div class="image">
		<img src="images/home-carousal.png" width="100%">
	</div>  -->


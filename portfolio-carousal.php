<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" data-aos="flip-up">
  
  <div class="carousel-inner">
    
    <?php
      $dirname = "images/new/home_page/";
			$images = glob($dirname."*.jpg");
			foreach($images as $key => $image) {
				if($key==0){
					$active = ' active';
				}else{
					$active = '';
				}

			    echo '<div class="carousel-item'.$active.'" style="background: #000;">
      <img src="'.$image.'" class="d-block w-100 w-banner-small" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="carousel-h1" data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="1200" style="font-weight: normal;">"'.$title.'"</h1>
        
      </div>
    </div>';
			}

		?>

    
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
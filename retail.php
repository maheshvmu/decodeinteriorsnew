<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>

	<div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">Browse through the collections</h2>
                        <p class="m-5 text-center">Creativity and innovation are the unmistakable features of Decode's style.<br> Refined contemporary design distinguishes domestic, office and outdoor settings.<br> Explore the collections, products and solutions designed to provide you with unique spaces.</p>
                        <p class="title text-center p-2 fs-1 fw-bold">Retail</p>
                        <p class="m-5 mt-3 text-center">Modern and high quality design that express comfort and elegance through the design language. Numerous versions give the house personality..</p>
                        
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<div class="row">
                    		<div class="col-md-12 col-sm-12 row">
                    			<?php
	                    			$dirname = "images/retail/";
									$images = glob($dirname."*.jpg");

									foreach($images as $image) {
									    

									    echo '<div class=" col-md-4 mb-2"><img class="title col-md-12" src="'.$image.'"><p class="text-center mt-2">Get Price</p></div>';
									}

								?>
                    			<!-- <div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news2.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news3.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news2.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news3.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news2.png"><p class="text-center mt-2">Get Price</p></div>
                    		</div>
                    		<div class="col-md-6 col-sm-12 row">
                    			
                    		</div>
                    		<div class="col-md-12 col-sm-12 row">
                    			
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>

                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div>
                    			<div class=" col-md-4 mb-2"><img class="title col-md-12" src="images/decode-news1.png"><p class="text-center mt-2">Get Price</p></div> -->
                    			
                    		</div>
                    		
                    	</div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 mt-5 mb-5" data-aos="fade-up">
                	<div class="row">

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Projects</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 style="margin-top: auto; margin-bottom: auto;" class="card-title align-middle">Design Culture</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Product Libraby</h3>
							    </div>
							  </div>
							</div>
                		</div>

                	</div>
                </div>
            </div>
        </div>
    </div>

    <?php  require('footer.php');  ?>

</body>
</html>
<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  $title = 'BRAND VISION';  ?>
	<?php  require('portfolio-carousal.php');  ?>
<style type="text/css">
    .carousel-caption {
          bottom: 35% !important;
          right: unset;
          left: unset;
          width: 100%;
          background-color: #00000088;
          padding-top: 2.25rem;
          padding-bottom: unset;
        }
</style>
	<div class="section">
		<div class="container">
			<div class="col-md-12 col-sm-12 row mt-5 mb-5" style="justify-content: center;">
    			<div class="col-md-12 col-sm-12 mt-3">
                    <div class="section-title row">
                    	
                    	<div class="col-md-12" >
                    		<img class="division" src="images/new/brandvision.jpg" width="100%"  data-aos="fade-right"></div> 
                    		<!-- <div class="col-md-6" data-aos="fade-left"> 
                    			<h4 class="title text-center p-2 pt-0">Brand Vision</h4> 
                    			<p class="m-5 mb-3 mt-2" style="text-align: justify;">Our vision is to provide high quality, personalised, artisanal furniture and decor that is made in India with the most rigorous specifications and attention to detail to our exacting and refined clientele. We believe in the intimacy of surrounding our lives with objects of beauty that are personal and hand crafted- a relationship between the devotion of a craft and the pleasure of each piece of furniture being unique and the product of crafts passed down through generations.</p> 
                    		</div> -->
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black" style="display: none;">
                <div class="col-md-12 col-sm-12 mt-3" data-aos="fade-up" style="display: none;">
                    <div class="section-title row">
                    	<div class="col-md-6" data-aos="fade-right"> 
                    		<h4 class="title text-center p-2">HOSPITALITY</h4>
                    			<p class="m-5 mb-3 text-center">DECODE’s expertise is ideally suited to hospitality and luxury projects due to the individuality and character of our product range, along with vast experience in this field with the highest quality of clients.</p> 
                    	</div>
                    	<div class="col-md-6" data-aos="fade-left">
                    		<img class="division" src="images/new/decode_division/Decode.jpg" width="100%">
                    	</div>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black" style="display: none;">
                <div class="col-md-12 col-sm-12 mt-3" data-aos="fade-up" style="display: none;">
                    <div class="section-title row">
                    	
                    	<div class="col-md-6" data-aos="fade-right">
                    		<img class="division" src="images/new/decode_division/Corporate.jpg" width="100%"></div> 
                    		<div class="col-md-6" data-aos="fade-left"> 
                    			<h4 class="title text-center p-2">CORPORATE</h4> 
                    			<p class="title text-center p-1">Contemporary international design</p> 
                    			<p class="m-5 mb-3 text-center">DECODE is ideally suited to a coporate clientele which seeks to break free from the monotony of mass-produced replicated designs and seeks to bring art, finesse and beauty to a professional environment. Our vast experience with a variety of SME and corporate clients across a variety of sectors make us your ideal partner to execute your dream office. </p> 
                    		</div>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black" style="display: none;">
                <div class="col-md-12 col-sm-12 mt-3" data-aos="fade-up" style="display: none;">
                    <div class="section-title row">
                    	<div class="col-md-6" data-aos="fade-right"> 
                    		<h4 class="title text-center p-2">RESIDENCE</h4>
                    			<p class="m-5 mb-3 text-center">Tradition and authenticity enliven a collection of classical furnishings with powerful contemporary traits. Handcrafting expertise, exclusive details and precious materials characterise DECODE's pieces for sophisticated interior décor projects.</p> 
                    	</div>
                    	<div class="col-md-6" data-aos="fade-left">
                    		<img class="division" src="images/new/decode_division/residential.jpg" width="100%">
                    	</div>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black" style="display: none;">
    		</div>

		</div>
	</div>
<?php  require('footer.php');  ?>
</body>
</html>
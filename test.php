<style type="text/css">
   .border-none {
  border-collapse: collapse;
  border: none;
}

.border-none td {
  border: 1px solid black;
}

.border-none tr:first-child td {
  border-top: none;
}

.border-none tr:last-child td {
  border-bottom: none;
}

.border-none tr td:first-child {
  border-left: none;
}

.border-none tr td:last-child {
  border-right: none;
}
</style>

<table class="border-none">
   <tr>
      <td>Cell 1</td>
      <td>Cell 2</td>
      <td>Cell 3</td>
   </tr>
   <tr>
      <td>Cell 4</td>
      <td>Cell 5</td>
      <td>Cell 6</td>
   </tr>
   <tr>
      <td>Cell 7</td>
      <td>Cell 8</td>
      <td>Cell 9</td>
   </tr>
   <tr>
      <td>Cell 10</td>
      <td>Cell 11</td>
      <td>Cell 12</td>
   </tr>
</table>
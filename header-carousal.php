<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" data-aos="flip-up">
  
  <div class="carousel-inner">
    
    <?php
      $dirname = "images/new/home_page/";
			$images = glob($dirname."*.jpg");
      $slogan = ['Artisanal furniture for the refined and exacting customer.', 'Design and fabrication of the highest quality that is tailored to your needs.', 'The finest of Indian hand-crafted furniture made by highly skilled artisans.'];
			foreach($images as $key => $image) {
				if($key==0){
					$active = ' active';
				}else{
					$active = '';
				}

        if(isset($slogan[$key])){

        }else{
          $slogan = array_merge($slogan,$slogan);
        }

			    echo '<div class="carousel-item'.$active.'" style="background: #000;">
      <img src="'.$image.'" class="d-block w-100 w-banner" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h1 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="800" class="carousel-h1 mb-4">"'.$slogan[$key].'"</h1>
        <a data-aos="zoom-in" data-aos-easing="ease" data-aos-delay="1200" href="all-products.php" style="text-decoration:none; color:white;" class="halo-btn">OUR PRODUCTS</a>
      </div>
    </div>';
			}

		?>

    
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
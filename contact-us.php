<?php
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	     
	}
	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	     
	}

?>

<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>

	<div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">HEADQUARTERS </h2>
                        <p class="m-5 mt-4 text-center">lorem ipsum lorem ipsum, lorem <br>ipsum, 600018.</p>
                        
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<div class="row justify-content-between">

                    		<div class="col-md-4 col-sm-12 text-center">
                    			<h3>GENERAL ENQUIRES</h3>
                    			<p>info@DECODE.com</p>
                    			<p>+91 1254 154 124</p>
                    		</div>

                    		<div class="col-md-4 col-sm-12 text-center">
                    			<h3>SALES DEPARTMENT</h3>
                    			<p>sales@DECODE.com</p>
                    			<p>+91 1254 154 124</p>
                    		</div>

                    		<div class="col-md-4 col-sm-12 text-center">
                    			<h3>PR & PRESS RELATIONS</h3>
                    			<p>press@DECODE.com</p>
                    			<p>+91 1254 154 124</p>
                    		</div>
                    		
                    	</div>

                    </div>
                </div>

                <hr class="dotted mt-5 mb-5">

                <div class="col-md-9 col-sm-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                    	<h2 class="title text-center p-2">SEND US A MESSAGE </h2>
                    	<p class="text-center">If you have questions or need any general information our customer service team will be happy to assist you.</p>
                    	<form action="#" method="post">
	                    	<div class="row justify-content-between">
	                    		
	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<select name="question" class="form-control">
	                    				<option value="">Question (required)</option>
	                    				<option value="General Info Request">General Info Request</option>
	                    				<option value="Press Info Request">Press Info Request</option>
	                    				<option value="Stock Info Request">Stock Info Request</option>
	                    				<option value="Price Info Request">Price's Info Request</option>
	                    				<option value="Bespoke Info Request">Bespoke Info Request</option>
	                    			</select>
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<select name="occupation" class="form-control">
	                    				<option value="">Occupation (required)</option>
	                    				<option value="General Info Request">General Info Request</option>
	                    				<option value="Press Info Request">Press Info Request</option>
	                    				<option value="Stock Info Request">Stock Info Request</option>
	                    				<option value="Price Info Request">Price's Info Request</option>
	                    				<option value="Bespoke Info Request">Bespoke Info Request</option>
	                    			</select>
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<input class="form-control" type="text" name="fname" placeholder="First Name (required)">
	                    		</div>
	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<input class="form-control" type="text" name="lname" placeholder="Last Name (required)">
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<input class="form-control" type="text" name="company" placeholder="Company">
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<input class="form-control" type="email" name="email" placeholder="E-mail (required)">
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<select name="know-about" class="form-control">
	                    				<option value="">How did you hear about us</option>
	                    				<option value="General Info Request">General Info Request</option>
	                    				<option value="Press Info Request">Press Info Request</option>
	                    				<option value="Stock Info Request">Stock Info Request</option>
	                    				<option value="Price Info Request">Price's Info Request</option>
	                    				<option value="Bespoke Info Request">Bespoke Info Request</option>
	                    			</select>
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<input class="form-control" type="number" name="phone" placeholder="Phone">
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			<select name="country" class="form-control">
	                    				<option value="">Country (required)</option>
	                    				<option value="India">India</option>
	                    				<option value="England">England</option>
	                    				<option value="Italy">Italy</option>
	                    				<option value="France">France</option>
	                    				<option value="Netherland">Netherland</option>
	                    			</select>
	                    		</div>

	                    		<div class="col-md-12 col-sm-12 form-group mt-3">
	                    			<textarea class="form-control" name="message" placeholder="Message"></textarea>
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3">
	                    			
									  <input class="form-check-input" name="privacy" type="checkbox" value="yes" id="flexCheckIndeterminate">
									  <label class="form-check-label" for="flexCheckIndeterminate">
									    I HAVE READ AND ACCEPT YOUR PRIVACY POLICY*
									  
									
	                    		</div>

	                    		<div class="col-md-6 col-sm-12 form-group mt-3 d-flex justify-content-end">
	                    			<button type="submit" class="btn btn-primary mb-3 ">SEND</button>
	                    		</div>
	                    	</div>
                    	</form>

                    </div>
                </div>

                <div class="col-md-12 col-sm-12 mt-5 mb-5" data-aos="fade-up">
                	<div class="row">

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Projects</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 style="margin-top: auto; margin-bottom: auto;" class="card-title align-middle">Design Culture</h3>
							    </div>
							  </div>
							</div>
                		</div>

                		<div class="col-md-4 col-sm-12">
                			<div class="card bg-dark text-white">
							  <div style="background-color: #333;">
							    <div style="opacity:.5;">
							    	<img class="card-img" src="images/decode-news3.png" alt="" style="object-fit: cover; width: 100%; padding: 0%;">
							    </div>
							    <div class="card-img-overlay text-center">
							      <a href="#" class="stretched-link"></a>
							      <h3 class="card-title align-middle">Product Libraby</h3>
							    </div>
							  </div>
							</div>
                		</div>

                	</div>
                </div>
            </div>
        </div>
    </div>

    <?php  require('footer.php');  ?>

</body>
</html>
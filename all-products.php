<?php  ini_set('display_errors', 1);

require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  $title = 'ALL PRODUCTS';  ?>
	<?php  require('portfolio-carousal.php');  
	$count = 0;
	?>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style1.css" type="text/css">


<style type="text/css">
  .navbar-light .navbar-nav .nav-link {
    color: rgba(0,0,0);
  }
</style>
  <div class="d-flex align-items-start bg-dark">
    <div class="nav flex-column nav-pills me-3 pt-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <h5 class="green-text text-center pt-2 pb-2" >PRODUCT TYPE</h5>
      <button class="nav-link active" style="color: #fff ;" id="v-pills-armchair-tab" data-bs-toggle="pill" data-bs-target="#v-pills-armchair" type="button" role="tab" aria-controls="v-pills-armchair" aria-selected="true">ARMCHAIRS</button>

      <button class="nav-link" style="color: #fff;" id="v-pills-accsor-tab" data-bs-toggle="pill" data-bs-target="#v-pills-accsor" type="button" role="tab" aria-controls="v-pills-accsor" aria-selected="false">ACCESSORIES</button>

      <button class="nav-link" style="color: #fff;" id="v-pills-beds-tab" data-bs-toggle="pill" data-bs-target="#v-pills-beds" type="button" role="tab" aria-controls="v-pills-beds" aria-selected="false">BEDS</button>

      <!-- <button class="nav-link" style="color: #fff;" id="v-pills-benches-tab" data-bs-toggle="pill" data-bs-target="#v-pills-benches" type="button" role="tab" aria-controls="v-pills-benches" aria-selected="false">BENCHES</button> -->

      <button class="nav-link" style="color: #fff;" id="v-pills-chairs-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chairs" type="button" role="tab" aria-controls="v-pills-chairs" aria-selected="false">CHAIRS</button>

      <!-- <button class="nav-link" style="color: #fff;" id="v-pills-complements-tab" data-bs-toggle="pill" data-bs-target="#v-pills-complements" type="button" role="tab" aria-controls="v-pills-complements" aria-selected="false">COMPLEMENTS</button> -->

      <button class="nav-link" style="color: #fff;" id="v-pills-chaise-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chaise" type="button" role="tab" aria-controls="v-pills-chaise" aria-selected="false">CHAISE LOUNGE</button>

      <!-- <button class="nav-link" style="color: #fff;" id="v-pills-office-tab" data-bs-toggle="pill" data-bs-target="#v-pills-office" type="button" role="tab" aria-controls="v-pills-office" aria-selected="false">EXECUTIVE OFFICE FURNITURE</button> -->

      <!-- <button class="nav-link" style="color: #fff;" id="v-pills-tables-tab" data-bs-toggle="pill" data-bs-target="#v-pills-tables" type="button" role="tab" aria-controls="v-pills-tables" aria-selected="false">TABLES</button> -->

      <button class="nav-link" style="color: #fff;" id="v-pills-kitchen-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kitchen" type="button" role="tab" aria-controls="v-pills-kitchen" aria-selected="false">KITCHEN FURNITURE</button>

      <!-- <button class="nav-link" style="color: #fff;" id="v-pills-smalltable-tab" data-bs-toggle="pill" data-bs-target="#v-pills-smalltable" type="button" role="tab" aria-controls="v-pills-smalltable" aria-selected="false">TABLES</button> -->

      <button class="nav-link" style="color: #fff;" id="v-pills-sofa-tab" data-bs-toggle="pill" data-bs-target="#v-pills-sofa" type="button" role="tab" aria-controls="v-pills-sofa" aria-selected="false">SOFAS</button>

      <button class="nav-link" style="color: #fff;" id="v-pills-study-chairs-tab" data-bs-toggle="pill" data-bs-target="#v-pills-study-chairs" type="button" role="tab" aria-controls="v-pills-study-chairs" aria-selected="false">STUDY CHAIRS</button>

    </div>

    <div class="tab-content bg-light" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-armchair" role="tabpanel" aria-labelledby="v-pills-armchair-tab">
       <div class="section">
         <div class="container">
          <div class="col-md-12 col-sm-12 row mt-5 mb-5">


            <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal">
              <?php
              $dirname = "images/new/all_products/armchairs/Armchairs/";
              $images = glob($dirname."*.jpg");
              foreach($images as $key => $image) {
                echo '<div class="col-12 col-sm-6 col-lg-4">
                <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample" data-slide-to="'.$key.'">
                </div>';
              }
              ?>

            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div id="carouselExample" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <?php  
                        foreach($images as $key => $image) {
                          if($key == 0){
                            echo '<li data-target="#carouselExample" data-slide-to="'.$key.'" class="active"></li>';
                          }else{
                            echo '<li data-target="#carouselExample" data-slide-to="'.$key.'" class=""></li>';
                          }
                        }
                        ?>

                      </ol>
                      <div class="carousel-inner">
                       <?php
                       $dirname = "images/new/all_products/armchairs/Armchairs - Details/";
                       $images = glob($dirname."*.jpg");
                       foreach($images as $key => $image) {
                         if($key == 0){
                           echo '<div class="carousel-item active">

                           <img class="d-block w-100" src="'.$image.'" alt="First slide">
                           </div>';
                         }else{
                          echo '<div class="carousel-item">

                          <img class="d-block w-100" src="'.$image.'" alt="First slide">
                          </div>';
                        }

                      }
                      ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>




        </div>

      </div>
    </div>

  </div>

  <div class="tab-pane fade" id="v-pills-accsor" role="tabpanel" aria-labelledby="v-pills-accsor-tab">
   <div class="section">
     <div class="container">
      <div class="col-md-12 col-sm-12 row mt-5 mb-5">

       <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal1">
         <?php
         $dirname = "images/new/all_products/accessories/Accessories/";
         $images = glob($dirname."*.jpg");
         foreach($images as $key => $image) {
          echo '<div class="col-12 col-sm-6 col-lg-4">
          <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample1" data-slide-to="'.$key.'">
          </div>';
        }
        ?>

      </div>

      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="carouselExample1" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                 <?php  foreach($images as $key => $image) {
                  if($key == 0){
                   echo '<li data-target="#carouselExample1" data-slide-to="'.$key.'" class="active"></li>';
                 }else{
                   echo '<li data-target="#carouselExample1" data-slide-to="'.$key.'" class=""></li>';
                 }
               }
               ?>

             </ol>
             <div class="carousel-inner">
               <?php
               $dirname = "images/new/all_products/accessories/Accessories Details/";
               $images = glob($dirname."*.jpg");
               foreach($images as $key => $image) {
                 if($key == 0){
                   echo '<div class="carousel-item active">

                   <img class="d-block w-100" src="'.$image.'" alt="First slide">
                   </div>';
                 }else{
                  echo '<div class="carousel-item">

                  <img class="d-block w-100" src="'.$image.'" alt="First slide">
                  </div>';
                }

              }
              ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExample1" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExample1" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-beds" role="tabpanel" aria-labelledby="v-pills-beds-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal2">
       <?php
       $dirname = "images/new/all_products/beds/Bed/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample2" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample2" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample2" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample2" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/beds/Bed details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-benches" role="tabpanel" aria-labelledby="v-pills-benches-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal3">
       <?php
       $dirname = "images/new/all_products/benches/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample3" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample3" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample3" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample3" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products_details/benches/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample3" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-chairs" role="tabpanel" aria-labelledby="v-pills-chairs-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal4">
       <?php
       $dirname = "images/new/all_products/chairs/Chairs/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample4" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample4" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample4" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample4" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/chairs/Chairs Details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample4" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample4" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-complements" role="tabpanel" aria-labelledby="v-pills-complements-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal5">
       <?php
       $dirname = "images/new/all_products/armchairs/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample5" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample5" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample5" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample5" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products_details/armchairs/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample5" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample5" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-chaise" role="tabpanel" aria-labelledby="v-pills-chaise-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal6">
       <?php
       $dirname = "images/new/all_products/chaise_lounge/Chaise Lounge/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample6" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample6" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample6" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample6" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/chaise_lounge/Chaise Lounge details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample6" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample6" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-office" role="tabpanel" aria-labelledby="v-pills-office-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal7">
       <?php
       $dirname = "images/new/all_products/study_chairs/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample7" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample7" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample7" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample7" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products_details/study_chairs/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample7" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample7" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-tables" role="tabpanel" aria-labelledby="v-pills-tables-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal8">
       <?php
       $dirname = "images/new/all_products/table/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample8" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal8" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample8" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample8" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample8" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products_details/table/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample8" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample8" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-kitchen" role="tabpanel" aria-labelledby="v-pills-kitchen-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal9">
       <?php
       $dirname = "images/new/all_products/kitchen/Kitchen/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample9" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal9" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample9" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample9" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample9" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/kitchen/Kitchen details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample9" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample9" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-smalltable" role="tabpanel" aria-labelledby="v-pills-smalltable-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal10">
       <?php
       $dirname = "images/new/all_products/armchairs/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample10" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal10" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample10" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample10" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample10" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products_details/armchairs/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample10" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample10" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-sofa" role="tabpanel" aria-labelledby="v-pills-sofa-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal11">
       <?php
       $dirname = "images/new/all_products/sofas/Sofa/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample11" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample11" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample11" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample11" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/sofas/Sofa Details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample11" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample11" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>
</div>

<div class="tab-pane fade" id="v-pills-study-chairs" role="tabpanel" aria-labelledby="v-pills-study-chairs-tab">
 <div class="section">
   <div class="container">
    <div class="col-md-12 col-sm-12 row mt-5 mb-5">
     <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal12">
       <?php
       $dirname = "images/new/all_products/study_chairs/Study chairs/";
       $images = glob($dirname."*.jpg");
       foreach($images as $key => $image) {
        echo '<div class="col-12 col-sm-6 col-lg-4">
        <img class="w-100 m-4 shadow" src="'.$image.'" alt="First slide" data-target="#carouselExample12" data-slide-to="'.$key.'">
        </div>';
      }
      ?>

    </div>

    <div class="modal fade" id="exampleModal12" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="carouselExample12" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
               <?php  foreach($images as $key => $image) {
                if($key == 0){
                 echo '<li data-target="#carouselExample12" data-slide-to="'.$key.'" class="active"></li>';
               }else{
                 echo '<li data-target="#carouselExample12" data-slide-to="'.$key.'" class=""></li>';
               }
             }
             ?>

           </ol>
           <div class="carousel-inner">
             <?php
             $dirname = "images/new/all_products/study_chairs/Study chairs Details/";
             $images = glob($dirname."*.jpg");
             foreach($images as $key => $image) {
               if($key == 0){
                 echo '<div class="carousel-item active">

                 <img class="d-block w-100" src="'.$image.'" alt="First slide">
                 </div>';
               }else{
                echo '<div class="carousel-item">

                <img class="d-block w-100" src="'.$image.'" alt="First slide">
                </div>';
              }

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExample12" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample12" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>
</div>

</div>
</div>

	<!-- <div class="section">
		<div class="container">
			<div class="col-md-12 col-sm-12 row mt-5 mb-5">
				<?php
				$dirname = "images/new/portfolio/";
				$images = glob($dirname."*.jpg");
				foreach($images as $key => $image) {
					echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
				}
				?>
			</div>
		</div>
	</div> -->


  <?php  require('footer.php');  ?>
  <script type="text/javascript">
   function switchStyle() {
    if (document.getElementById('styleSwitch').checked) {
      document.getElementById('gallery').classList.add("custom");
      document.getElementById('exampleModal').classList.add("custom");
    } else {
      document.getElementById('gallery').classList.remove("custom");
      document.getElementById('exampleModal').classList.remove("custom");
    }
  }
</script>
</body>
</html>
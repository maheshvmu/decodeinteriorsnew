<?php  ini_set('display_errors', 1);

require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  $title = 'ALL PRODUCTS';  ?>
	<?php  require('portfolio-carousal.php');  
	$count = 0;
	?>

    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="style1.css" type="text/css">
    
    
    

	<div class="d-flex align-items-start bg-dark">
  <div class="nav flex-column nav-pills me-3 pt-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <h5 class="green-text text-center pt-2 pb-2">PRODUCT TYPE</h5>
    <button class="nav-link active" id="v-pills-armchair-tab" data-bs-toggle="pill" data-bs-target="#v-pills-armchair" type="button" role="tab" aria-controls="v-pills-armchair" aria-selected="true">ARMCHAIRS</button>

    <button class="nav-link" id="v-pills-accsor-tab" data-bs-toggle="pill" data-bs-target="#v-pills-accsor" type="button" role="tab" aria-controls="v-pills-accsor" aria-selected="false">ACCESSORIES</button>

    <button class="nav-link" id="v-pills-beds-tab" data-bs-toggle="pill" data-bs-target="#v-pills-beds" type="button" role="tab" aria-controls="v-pills-beds" aria-selected="false">BEDS</button>

    <button class="nav-link" id="v-pills-benches-tab" data-bs-toggle="pill" data-bs-target="#v-pills-benches" type="button" role="tab" aria-controls="v-pills-benches" aria-selected="false">BENCHES</button>

    <button class="nav-link" id="v-pills-chairs-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chairs" type="button" role="tab" aria-controls="v-pills-chairs" aria-selected="false">CHAIRS</button>

    <button class="nav-link" id="v-pills-complements-tab" data-bs-toggle="pill" data-bs-target="#v-pills-complements" type="button" role="tab" aria-controls="v-pills-complements" aria-selected="false">COMPLEMENTS</button>

    <button class="nav-link" id="v-pills-chaise-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chaise" type="button" role="tab" aria-controls="v-pills-chaise" aria-selected="false">CHAISE LOUNGE</button>

    <button class="nav-link" id="v-pills-office-tab" data-bs-toggle="pill" data-bs-target="#v-pills-office" type="button" role="tab" aria-controls="v-pills-office" aria-selected="false">EXECUTIVE OFFICE FURNITURE</button>

    <button class="nav-link" id="v-pills-tables-tab" data-bs-toggle="pill" data-bs-target="#v-pills-tables" type="button" role="tab" aria-controls="v-pills-tables" aria-selected="false">TABLES</button>

    <button class="nav-link" id="v-pills-kitchen-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kitchen" type="button" role="tab" aria-controls="v-pills-kitchen" aria-selected="false">KITCHEN FURNITURE</button>

    <button class="nav-link" id="v-pills-smalltable-tab" data-bs-toggle="pill" data-bs-target="#v-pills-smalltable" type="button" role="tab" aria-controls="v-pills-smalltable" aria-selected="false">SMALL TABLES</button>

    <button class="nav-link" id="v-pills-sofa-tab" data-bs-toggle="pill" data-bs-target="#v-pills-sofa" type="button" role="tab" aria-controls="v-pills-sofa" aria-selected="false">SOFAS</button>

  </div>

  <div class="tab-content bg-light" id="v-pills-tabContent">
    <div class="tab-pane fade show active" id="v-pills-armchair" role="tabpanel" aria-labelledby="v-pills-armchair-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">


<!-- Gallery -->
<!-- 
Gallery is linked to lightbox using data attributes.

To open lightbox, this is added to the gallery element: {data-toggle="modal" data-target="#exampleModal"}.

To open carousel on correct image, this is added to each image element: {data-target="#carouselExample" data-slide-to="0"}.
Replace '0' with corresponding slide number.
-->

<div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal">
	<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<div class="col-12 col-sm-6 col-lg-4">
    <img class="w-100" src="'.$image.'" alt="First slide" data-target="#carouselExample" data-slide-to="0">
  </div>';
						}
					?>
  
</div>

<!-- Modal -->
<!-- 
This part is straight out of Bootstrap docs. Just a carousel inside a modal.
-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselExample" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
          	<?php  foreach($images as $key => $image) {
          		if($key == 0){
          			echo '<li data-target="#carouselExample" data-slide-to="'.$key.'" class="active"></li>';
          		}else{
          			echo '<li data-target="#carouselExample" data-slide-to="'.$key.'" class=""></li>';
          		}
          	}
          	?>
            <!-- <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExample" data-slide-to="1"></li>
            <li data-target="#carouselExample" data-slide-to="2"></li>
            <li data-target="#carouselExample" data-slide-to="3"></li> -->
          </ol>
          <div class="carousel-inner">
          	<?php
	        			$dirname = "images/new/all_products_details/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $key => $image) {
							if($key == 0){
						  	echo '<div class="carousel-item active">
							
              	<img class="d-block w-100" src="'.$image.'" alt="First slide">
            		</div>';
            	}else{
            		echo '<div class="carousel-item">
							
              	<img class="d-block w-100" src="'.$image.'" alt="First slide">
            		</div>';
            	}

						}
					?>
            <!-- <div class="carousel-item active">
              <img class="d-block w-100" src="https://images.unsplash.com/photo-1546853020-ca4909aef454?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0NTg5fQ" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://images.unsplash.com/photo-1546534505-d534e27ecb35?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0NTg5fQ" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://images.unsplash.com/photo-1546111380-cfca9a43dd85?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0NTg5fQ" alt="Third slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://images.unsplash.com/photo-1547288242-f3d375fc7b5f?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE0NTg5fQ" alt="Fourth slide">
            </div> -->
          </div>
          <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



	    			
	    		</div>

			</div>
		</div>

    </div>

    <div class="tab-pane fade" id="v-pills-accsor" role="tabpanel" aria-labelledby="v-pills-accsor-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-beds" role="tabpanel" aria-labelledby="v-pills-beds-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/beds/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-benches" role="tabpanel" aria-labelledby="v-pills-benches-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/benches/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-chairs" role="tabpanel" aria-labelledby="v-pills-chairs-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/study_chairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-complements" role="tabpanel" aria-labelledby="v-pills-complements-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-chaise" role="tabpanel" aria-labelledby="v-pills-chaise-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-office" role="tabpanel" aria-labelledby="v-pills-office-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/study_chairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-tables" role="tabpanel" aria-labelledby="v-pills-tables-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/table/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-kitchen" role="tabpanel" aria-labelledby="v-pills-kitchen-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-smalltable" role="tabpanel" aria-labelledby="v-pills-smalltable-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-sofa" role="tabpanel" aria-labelledby="v-pills-sofa-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/sofas/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>
  </div>
</div>

	<!-- <div class="section">
		<div class="container">
			<div class="col-md-12 col-sm-12 row mt-5 mb-5">
				<?php
				$dirname = "images/new/portfolio/";
				$images = glob($dirname."*.jpg");
				foreach($images as $image) {
					echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
				}
				?>
			</div>
		</div>
	</div> -->


<?php  require('footer.php');  ?>
<script type="text/javascript">
	function switchStyle() {
  if (document.getElementById('styleSwitch').checked) {
    document.getElementById('gallery').classList.add("custom");
    document.getElementById('exampleModal').classList.add("custom");
  } else {
    document.getElementById('gallery').classList.remove("custom");
    document.getElementById('exampleModal').classList.remove("custom");
  }
}
</script>
</body>
</html>
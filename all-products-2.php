<?php  ini_set('display_errors', 1);

require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  $title = 'ALL PRODUCTS';  ?>
	<?php  require('portfolio-carousal.php');  
	$count = 0;
	?>
	<div class="d-flex align-items-start bg-dark">
  <div class="nav flex-column nav-pills me-3 pt-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <h5 class="green-text text-center pt-2 pb-2">PRODUCT TYPE</h5>
    <button class="nav-link active" id="v-pills-armchair-tab" data-bs-toggle="pill" data-bs-target="#v-pills-armchair" type="button" role="tab" aria-controls="v-pills-armchair" aria-selected="true">ARMCHAIRS</button>

    <button class="nav-link" id="v-pills-accsor-tab" data-bs-toggle="pill" data-bs-target="#v-pills-accsor" type="button" role="tab" aria-controls="v-pills-accsor" aria-selected="false">ACCESSORIES</button>

    <button class="nav-link" id="v-pills-beds-tab" data-bs-toggle="pill" data-bs-target="#v-pills-beds" type="button" role="tab" aria-controls="v-pills-beds" aria-selected="false">BEDS</button>

    <button class="nav-link" id="v-pills-benches-tab" data-bs-toggle="pill" data-bs-target="#v-pills-benches" type="button" role="tab" aria-controls="v-pills-benches" aria-selected="false">BENCHES</button>

    <button class="nav-link" id="v-pills-chairs-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chairs" type="button" role="tab" aria-controls="v-pills-chairs" aria-selected="false">CHAIRS</button>

    <button class="nav-link" id="v-pills-complements-tab" data-bs-toggle="pill" data-bs-target="#v-pills-complements" type="button" role="tab" aria-controls="v-pills-complements" aria-selected="false">COMPLEMENTS</button>

    <button class="nav-link" id="v-pills-chaise-tab" data-bs-toggle="pill" data-bs-target="#v-pills-chaise" type="button" role="tab" aria-controls="v-pills-chaise" aria-selected="false">CHAISE LOUNGE</button>

    <button class="nav-link" id="v-pills-office-tab" data-bs-toggle="pill" data-bs-target="#v-pills-office" type="button" role="tab" aria-controls="v-pills-office" aria-selected="false">EXECUTIVE OFFICE FURNITURE</button>

    <button class="nav-link" id="v-pills-tables-tab" data-bs-toggle="pill" data-bs-target="#v-pills-tables" type="button" role="tab" aria-controls="v-pills-tables" aria-selected="false">TABLES</button>

    <button class="nav-link" id="v-pills-kitchen-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kitchen" type="button" role="tab" aria-controls="v-pills-kitchen" aria-selected="false">KITCHEN FURNITURE</button>

    <button class="nav-link" id="v-pills-smalltable-tab" data-bs-toggle="pill" data-bs-target="#v-pills-smalltable" type="button" role="tab" aria-controls="v-pills-smalltable" aria-selected="false">SMALL TABLES</button>

    <button class="nav-link" id="v-pills-sofa-tab" data-bs-toggle="pill" data-bs-target="#v-pills-sofa" type="button" role="tab" aria-controls="v-pills-sofa" aria-selected="false">SOFAS</button>

  </div>

  <div class="tab-content bg-light" id="v-pills-tabContent">
    <div class="tab-pane fade show active" id="v-pills-armchair" role="tabpanel" aria-labelledby="v-pills-armchair-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" data-toggle="modal" data-target="#largeModal'.$count.'" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>

    </div>

    <div class="tab-pane fade" id="v-pills-accsor" role="tabpanel" aria-labelledby="v-pills-accsor-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-beds" role="tabpanel" aria-labelledby="v-pills-beds-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/beds/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-benches" role="tabpanel" aria-labelledby="v-pills-benches-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/benches/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-chairs" role="tabpanel" aria-labelledby="v-pills-chairs-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/study_chairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-complements" role="tabpanel" aria-labelledby="v-pills-complements-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-chaise" role="tabpanel" aria-labelledby="v-pills-chaise-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-office" role="tabpanel" aria-labelledby="v-pills-office-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/study_chairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-tables" role="tabpanel" aria-labelledby="v-pills-tables-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/table/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-kitchen" role="tabpanel" aria-labelledby="v-pills-kitchen-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-smalltable" role="tabpanel" aria-labelledby="v-pills-smalltable-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/armchairs/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>

    <div class="tab-pane fade" id="v-pills-sofa" role="tabpanel" aria-labelledby="v-pills-sofa-tab">
    	<div class="section">
			<div class="container">
				<div class="col-md-12 col-sm-12 row mt-5 mb-5">
	    			<?php
	        			$dirname = "images/new/all_products/sofas/";
						$images = glob($dirname."*.jpg");
						foreach($images as $image) {
						    echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
						}
					?>
	    		</div>

			</div>
		</div>
    </div>
  </div>
</div>

	<!-- <div class="section">
		<div class="container">
			<div class="col-md-12 col-sm-12 row mt-5 mb-5">
				<?php
				$dirname = "images/new/portfolio/";
				$images = glob($dirname."*.jpg");
				foreach($images as $image) {
					echo '<img class="title col-md-3 m-4 shadow" src="'.$image.'">';
				}
				?>
			</div>
		</div>
	</div> -->


<?php  require('footer.php');  ?>
</body>
</html>
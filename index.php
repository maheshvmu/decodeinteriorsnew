<?php  require('header.php');  ?>
<body>
	<?php  require('navbar.php');  ?>
	<?php  require('header-carousal.php');  ?>

	<div class="section">
        <div class="mx-1">

        	<div class="col-md-12 mt-5" data-aos="fade-up">
                    <div class="section-title">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
						  <div class="carousel-inner">
						    <div class="carousel-item active">
							    <div class="col-md-12 mt-5" data-aos="zoom-in">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal w-equal" src="images/new/gallery/1.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal w-equal" src="images/new/gallery/2.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/3.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/4.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/5.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/6.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/7.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/8.jpg">
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						    </div>

						    <div class="carousel-item">
						    	<div class="col-md-12 mt-5" data-aos="zoom-in">
				                    <div class="section-title">
				                    	<div class="row">
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/9.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/10.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/11.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/12.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/13.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/14.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/15.jpg">
				                    		</div>
				                    		<div class="col-md-3 col-sm-3 p-2">
				                    			<img class="w-100 w-equal" src="images/new/gallery/16.jpg">
				                    		</div>
				                    	</div>
				                    </div>
				                    
				                </div>
						      <!-- <img src="decode-img3.png" class="d-block w-100" alt="..."> -->
						    </div>
						  </div>
						  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Previous</span>
						  </button>
						  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="visually-hidden">Next</span>
						  </button>
						</div>
<br>
						<center class="mb-4"><a href="portfolio.php" class="text-center fs-5 green-btn">SEE MORE</a></center>
                    </div>
                    
                </div>
            </div>
        	<div class="container">
            <div class="row justify-content-center">
            	<div class="col-md-2"></div>
                <div class="col-md-8 col-sm-12 mt-3" data-aos="fade-down">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">Welcome to DECODE</h4>
                        <p class="m-5 text-center">Decode is born of the desire to create tasteful, handmade and intimate luxury furniture solutions for our sophisticated clientele. We bring together the finest of Indian artisanal furniture and artifacts that we produce with the highest attention to craft and detail, employing our over 30 years of experience and relationships in the architectural and construction fields.</p>
                    </div>
                </div>
                <div class="col-md-2 mt-5"></div>

                <nav class="mt-5" data-aos="fade-left">
				  <div class="nav nav-tabs" id="nav-tab" role="tablist">
				    <button class="nav-link green-text fw-bold" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-consultation" type="button" role="tab" aria-controls="nav-home" aria-selected="true">CONSULTATION</button>
				    <button class="nav-link green-text fw-bold" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-ideation" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">IDEATION</button>
				    <button class="nav-link green-text fw-bold" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-fabrication" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">FABRICATION</button>
				    <button class="nav-link green-text fw-bold" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-installation" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">INSTALLATION</button>
				  </div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
				  	<div class="tab-pane fade" id="nav-consultation" role="tabpanel" aria-labelledby="nav-home-tab">
				  		<div class="col-md-12 mt-5" data-aos="fade-up">
				  			<hr class="color-black mt-5">
		                    <div class="section-title">
		                    	<div class="row">
		                    		<div class="col-md-4 col-sm-3 p-2">
		                    			<img class="w-100 w-equal" src="images/new/four_box/CONSULTATION.jpg">
		                    		</div>
		                    		<div class="col-md-8 col-sm-3 p-2">
		                    			<h5 class="green-text">CONSULTATION</h5>
		                    			<p>Our personal one on one consultation service with our clients where we understand our client’s needs and desires.</p>
		                    		</div>
		                    	</div>
		                    </div> 
		                </div>
				  	</div>
				  	<div class="tab-pane fade" id="nav-ideation" role="tabpanel" aria-labelledby="nav-profile-tab">
				  		<div class="col-md-12 mt-5" data-aos="fade-up">
				  			<hr class="color-black mt-5">
		                    <div class="section-title">
		                    	<div class="row">
		                    		<div class="col-md-4 col-sm-3 p-2">
		                    			<img class="w-100 w-equal" src="images/new/four_box/IDEATION.jpg">
		                    		</div>
		                    		<div class="col-md-8 col-sm-3 p-2">
		                    			<h5 class="green-text">IDEATION</h5>
		                    			<p>We devise a tailor made solution after considering our resources for our client’s needs.</p>
		                    		</div>
		                    	</div>
		                    </div> 
		                </div>
				  	</div>
				  	<div class="tab-pane fade" id="nav-fabrication" role="tabpanel" aria-labelledby="nav-contact-tab">
				  		<div class="col-md-12 mt-5" data-aos="fade-up">
				  			<hr class="color-black mt-5">
		                    <div class="section-title">
		                    	<div class="row">
		                    		<div class="col-md-4 col-sm-3 p-2">
		                    			<img class="w-100 w-equal" src="images/new/four_box/FABRICATION.jpg">
		                    		</div>
		                    		<div class="col-md-8 col-sm-3 p-2">
		                    			<h5 class="green-text">FABRICATION</h5>
		                    			<p>We develop the required furniture for you to the highest quality at our artisanal factories and workshops across India along with any personalized customizations necessary specific to project.</p>
		                    		</div>
		                    	</div>
		                    </div> 
		                </div>
				  	</div>
				  	<div class="tab-pane fade" id="nav-installation" role="tabpanel" aria-labelledby="nav-contact-tab">
				  		<div class="col-md-12 mt-5" data-aos="fade-up">
				  			<hr class="color-black mt-5">
		                    <div class="section-title">
		                    	<div class="row">
		                    		<div class="col-md-4 col-sm-3 p-2">
		                    			<img class="w-100 w-equal" src="images/new/four_box/INSTALLATION.jpg">
		                    		</div>
		                    		<div class="col-md-8 col-sm-3 p-2">
		                    			<h5 class="green-text">INSTALLATION</h5>
		                    			<p>We deliver the products to you through one of our exclusive logistical partners and arrange for it to be installed at your location.</p>
		                    		</div>
		                    	</div>
		                    </div> 
		                </div>
				  	</div>
				</div>

				<hr class="color-black mt-5">

                <div class="col-md-8 col-sm-12 mt-3" data-aos="fade-up">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">DECODE</h4>
                    	<p class="title text-center p-1">Contemporary international design</p>
                    	<center><img class="division" src="images/new/decode_division/Decode.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Contemporary design and timeless elegance. DECODE interprets contemporary culture and the evolution of living trends with premium quality furnishings designed to enhance indoor and outdoor areas. Timeless objects, international icons for design enthusiasts.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-3" data-aos="fade-up">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">HOSPITALITY</h4>
                    	<center><img class="division" src="images/new/decode_division/Hospitality.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">DECODE’s expertise is ideally suited to hospitality and luxury projects due to the individuality and character of our product range, along with vast experience in this field with the highest quality of clients.</p>
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-3" data-aos="fade-up">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">CORPORATE</h4>
                    	<center><img class="division" src="images/new/decode_division/Corporate.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">DECODE is ideally suited to a coporate clientele which seeks to break free from the monotony of mass-produced replicated designs and seeks to bring art, finesse and beauty to a professional environment. Our vast experience with a variety of SME and corporate clients across a variety of sectors make us your ideal partner to execute your dream office. </p>
                    </div>
                </div>
                    <hr class="color-black">
                <div class="col-md-8 col-sm-12 mt-3" data-aos="fade-up">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">RESIDENCE</h4>
                    	<p class="title text-center p-1">Contemporary classic</p>
                    	<center><img class="division" src="images/new/decode_division/residential.jpg" width="100%"></center>
                        <p class="m-5 mb-3 text-center">Tradition and authenticity enliven a collection of classical furnishings with powerful contemporary traits. Handcrafting expertise, exclusive details and precious materials characterise DECODE's pieces for sophisticated interior décor projects.</p>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">Classique</a></center> -->
                    </div>
                </div>
                    <hr class="color-black">

                <div class="col-md-12 col-sm-12 mt-3" data-aos="fade-up" id="clients">
                    <div class="section-title">
                    	<h4 class="title text-center p-2 pb-5">OUR CLIENTS</h4>
                    	<style type="text/css">
   .border-none {
  border-collapse: collapse;
  border: none;
}

.border-none td {
  border: 1px solid black;
}

.border-none tr:first-child td {
  border-top: none;
}

.border-none tr:last-child td {
  border-bottom: none;
}

.border-none tr td:first-child {
  border-left: none;
}

.border-none tr td:last-child {
  border-right: none;
}
</style>
<center>
<table class="border-none" style="width:100%">
   <tr>
      <td><center><img data-aos="flip-left" class="client_img" src="images/new/client/1.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="400" class="client_img" src="images/new/client/2.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="800" class="client_img" src="images/new/client/3.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="1200" class="client_img" src="images/new/client/4.jpg"></center></td>
   </tr>
   <tr>
      <td><center><img data-aos="flip-left" class="client_img" src="images/new/client/5.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="400" class="client_img" src="images/new/client/6.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="800" class="client_img" src="images/new/client/7.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="1200" class="client_img" src="images/new/client/8.jpg"></center></td>
   </tr>
   <tr>
      <td><center><img data-aos="flip-left" class="client_img" src="images/new/client/9.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="400" class="client_img" src="images/new/client/10.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="800" class="client_img" src="images/new/client/11.jpg"></center></td>
      <td><center><img data-aos="flip-left" data-aos-easing="ease" data-aos-delay="1200" class="client_img" src="images/new/client/12.jpg"></center></td>
   </tr>
</table></center>
<br>
                    </div>
                </div>

                
                    <hr class="color-black">
                <!-- <div class="col-md-12 mt-3" data-aos="fade-up">
                    <div class="section-title">
                    	<h4 class="title text-center p-2">OUR TEAM</h4>
                    	<div class="row justify-content-center">
                    		<div class="team-box col-md-3 col-sm-12">
							  <img data-aos="flip-left" src="images/new/our_team/ks.jpg" class="image w-100">
							  <div class="middle">
							  	<br>
							  	<br>
							    <div class="name-red">KRITHIKA SUBRAHMANIAN</div>
							    <small class="text">FOUNDER | MANAGING DIRECTOR | TRANSFORM DESIGN PVT. LTD. | SRESHTA LEISURE PVT. LTD.,</small>
							  </div>
							</div>
                    		<div class="team-box col-md-3 col-sm-12">
							  <img data-aos="flip-left" src="images/new/our_team/sriman.jpg" class="image w-100">
							  <div class="middle">
							    <div class="name-red">SRIMAN SUBRAMANIAN</div>
							    <small class="text">DIRECTOR</small>
							  </div>
							</div>
							<div class="team-box col-md-3 col-sm-12">
							  <img data-aos="flip-left" src="images/new/our_team/sriram.jpg" class="image w-100">
							  <div class="middle">
							    <div class="name-red">SRIRAM SUBRAMANIAN</div>
							    <small class="text">DIRECTOR</small>
							  </div>
							</div>
                    	</div>
						
                    </div>
                    <hr class="color-black">
                </div> -->
                <div class="col-md-12 col-sm-12 mt-3">
                    <div class="section-title row">
                    	
                    	<div class="col-md-3" >
                    		<img class="division" src="images/new/our_team/sriram-1.png" height="100%"  data-aos="fade-right"></div> 
                    		<div class="col-md-9 promotor" data-aos="fade-left"> 
                    			<h4 class="title text-left green-text pt-0 px-5" style="margin-bottom: 1px;">SRIMAN SUBRAMANIAN</h4> 
                    			<h5 class="title text-left green-text pt-0 px-5">Promoter</h5> 
                    			<p class="m-5 mb-3 mt-2" style="text-align: justify;">Sriman is focused on Design Management and developmental work in Architectural projects and products, international marketing and trade, developing capabilities across the globe in high quality luxury products and unique projects of sustainable, environmentally sensitive and adaptive value. Proficient in French, Italian, Farsi and Arabic.</p> 
                    		</div>
						<!-- <center class="mb-4"><a class="text-center fs-5 pt-3" href="#">DECODE</a></center> -->
                    </div>
                </div>
                <hr class="color-black">
            </div>
        </div>
    </div>



    <?php  require('footer.php');  ?>
    <!-- <script type="text/javascript">
  //   	$(".nav-link").hover(function(){
		//     $(this).tab('show');
		// });
    	$(".nav-link").mouseover(function() {
		    $(this).tab('show');
		}).mouseout(function() {
		    $(this).tab('hide');
		});
    </script> -->
</body>
</html>